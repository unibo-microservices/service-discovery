# set base image (host OS)
FROM python:3.8-alpine

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install --upgrade pip setuptools wheel
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working directory
COPY app.py .

EXPOSE 5500

# command to run on container start
CMD [ "python", "./app.py" ]
