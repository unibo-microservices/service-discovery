from flask import Flask
import redis

app = Flask(__name__)
r = redis.Redis(host='redis', port=6380)


@app.route('/')
def get_services():
    # r = redis.Redis(host='redis', port=6600, db=0)  # docker-compose
    # keys = r.keys('*')
    s = r.get('service1')
    return {
        "service": str(s),
    }


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5500)

